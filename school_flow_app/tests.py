from django.test import TestCase
from django.contrib.auth.models import Group
from .models import User
from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse


class UserModelTest(TestCase):
    def setUp(self):
        self.group = Group.objects.create(name='Test Role')
        self.user = User.objects.create_user(id=1, email='test@example.com', name='John', surname='Doe',
                                             password='test123',
                                             role='Test Role')

    def test_user_creation(self):
        group = Group.objects.get(name='Test Role')

        self.assertEqual(self.user.email, 'test@example.com')
        self.assertEqual(self.user.name, 'John')
        self.assertEqual(self.user.surname, 'Doe')
        self.assertTrue(self.user.check_password('test123'))
        self.assertTrue(self.user.is_active)
        self.assertFalse(self.user.is_staff)
        self.assertFalse(self.user.is_superuser)
        self.assertIn(group, self.user.groups.all())


class LoginAPITestCase(APITestCase):
    def setUp(self):
        self.url = reverse('login')
        self.user = User.objects.create_user(id=1, email='test@example.com', name='John', surname='Doe',
                                             password='test123',
                                             role='Test Role')

    def test_login_with_valid_credentials(self):
        data = {
            'email': 'test@example.com',
            'password': 'test123'
        }
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('access', response.data)
        self.assertIn('refresh', response.data)

    def test_login_with_invalid_credentials(self):
        data = {
            'email': 'test@example.com',
            'password': 'wrong'
        }
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data['detail'], 'Invalid credentials')


class UserModelTest(TestCase):
    def setUp(self):
        self.group = Group.objects.create(name='Test Role')
        self.user = User.objects.create_user(id=1, email='test@example.com', name='John', surname='Doe',
                                             password='test123',
                                             role='Test Role')

    def test_user_creation(self):
        group = Group.objects.get(name='Test Role')

        self.assertEqual(self.user.email, 'test@example.com')
        self.assertEqual(self.user.name, 'John')
        self.assertEqual(self.user.surname, 'Doe')
        self.assertTrue(self.user.check_password('test123'))
        self.assertTrue(self.user.is_active)
        self.assertFalse(self.user.is_staff)
        self.assertFalse(self.user.is_superuser)
        self.assertIn(group, self.user.groups.all())
