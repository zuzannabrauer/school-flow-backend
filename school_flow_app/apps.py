from django.apps import AppConfig


class SchoolFlowAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'school_flow_app'
